package mocks

import (
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"
	"github.com/stretchr/testify/mock"
)

type ServiceMock struct {
	mock.Mock
}

func (_m *ServiceMock) Get() domain.User {
	args := _m.Called()

	return args.Get(0).(domain.User)
}
