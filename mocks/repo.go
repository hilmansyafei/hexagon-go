package mocks

import (
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"
	"github.com/stretchr/testify/mock"
)

type RepoMock struct {
	mock.Mock
}

func (_m *RepoMock) Get() domain.User {
	args := _m.Called()

	return args.Get(0).(domain.User)
}
