module bitbucket.org/hilmansyafei/go-mod-test

go 1.12

require (
	github.com/julienschmidt/httprouter v1.3.0
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.7.0
)
