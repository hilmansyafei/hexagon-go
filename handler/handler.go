package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/ports"
	"github.com/julienschmidt/httprouter"
)

type handler struct {
	userService ports.UserService
}

func NewHandler(usr ports.UserService) *handler {
	return &handler{
		userService: usr,
	}
}

func (h *handler) Get(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	user := h.userService.Get()
	json.NewEncoder(w).Encode(user)
}
