package handler

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/ports"
	"bitbucket.org/hilmansyafei/go-mod-test/mocks"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
)

func TestNewHandler(t *testing.T) {
	type args struct {
		userService ports.UserService
	}
	tests := []struct {
		name string
		args args
		want *handler
	}{
		{
			name: "success",
			args: args{
				userService: nil,
			},
			want: &handler{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := NewHandler(tt.args.userService)
			assert.Equal(t, h, tt.want)
		})
	}
}

func TestGet(t *testing.T) {
	usesrv := new(mocks.ServiceMock)

	ret := domain.User{
		ID:    "1",
		Name:  "t",
		Value: 10,
	}

	usesrv.On("Get").Return(ret)

	// gen handler
	h := NewHandler(usesrv)

	// gen router
	router := httprouter.New()
	router.GET("/get", h.Get)

	// gen http
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/get", nil)
	router.ServeHTTP(rec, req)

	assert.Equal(t, 200, rec.Code)

	usr := domain.User{}
	json.Unmarshal(rec.Body.Bytes(), &usr)
	assert.Equal(t, "1", usr.ID)
}
