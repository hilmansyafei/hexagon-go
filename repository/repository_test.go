package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewUserRepo(t *testing.T) {
	testcase := []struct {
		name string
		want *userRepository
	}{
		{
			name: "success",
			want: &userRepository{},
		},
	}

	for _, tt := range testcase {
		t.Run(tt.name, func(t *testing.T) {
			r := NewUserRepo()
			assert.Equal(t, r, tt.want)
			assert.Equal(t, r.Get().ID, "1")
		})
	}
}
