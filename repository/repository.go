package repository

import "bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"

type userRepository struct{}

func NewUserRepo() *userRepository {
	return &userRepository{}
}

func (urp *userRepository) Get() domain.User {
	return domain.User{
		ID:   "1",
		Name: "Hilman",
	}
}
