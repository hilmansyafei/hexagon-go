package ports

import "bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"

type UserRepository interface {
	Get() domain.User
}

type UserService interface {
	Get() domain.User
}
