package services

import (
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/ports"
)

type userService struct {
	userRepo ports.UserRepository
}

func NewUserService(usr ports.UserRepository) *userService {
	return &userService{
		userRepo: usr,
	}
}

func (usv *userService) Get() domain.User {
	user := usv.userRepo.Get()
	user.Value = 10
	return user
}
