package services

import (
	"testing"

	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/domain"
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/ports"
	"bitbucket.org/hilmansyafei/go-mod-test/mocks"
	"github.com/stretchr/testify/assert"
)

func TestNewUserService(t *testing.T) {

	testcase := []struct {
		name string
		args ports.UserRepository
		want *userService
	}{
		{
			name: "success",
			args: nil,
			want: &userService{},
		},
	}

	for _, tt := range testcase {
		t.Run(tt.name, func(t *testing.T) {
			usersrv := NewUserService(tt.args)
			assert.Equal(t, usersrv, tt.want)
		})
	}
}

func TestGet(t *testing.T) {
	userRp := new(mocks.RepoMock)

	ret := domain.User{
		ID:    "1",
		Name:  "t",
		Value: 10,
	}
	userRp.On("Get").Return(ret)
	usrsrv := NewUserService(userRp)

	assert.Equal(t, usrsrv.Get().ID, "1")

}
