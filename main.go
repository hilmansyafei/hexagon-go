package main

import (
	"fmt"
	"net/http"
	"sort"

	"bitbucket.org/hilmansyafei/go-mod-test/handler"
	"bitbucket.org/hilmansyafei/go-mod-test/internal/core/services"
	"bitbucket.org/hilmansyafei/go-mod-test/repository"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

func main() {
	userRepo := repository.NewUserRepo()

	userService := services.NewUserService(userRepo)

	handler := handler.NewHandler(userService)

	router := httprouter.New()
	router.GET("/test", handler.Get)

	co := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PATCH", "DELETE", "PUT", "HEAD", "OPTIONS"},
		AllowedHeaders: []string{"*"},
		MaxAge:         86400,
	})

	http.ListenAndServe(":8081", co.Handler(router))

	n := 1982345
	v := 10
	l := make([]int, 7)

	for i := 6; i >= 0; i-- {
		s := (n % v)
		s2 := s
		if s > 10 {
			s2 = s2 / (v / 10)
		}
		l[i] = s2

		n = n - s

		v *= 10
	}
	sort.Ints(l)
	fmt.Println(l)

}
